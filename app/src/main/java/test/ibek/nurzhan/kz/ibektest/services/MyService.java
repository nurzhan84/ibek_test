package test.ibek.nurzhan.kz.ibektest.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.ArrayMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import test.ibek.nurzhan.kz.ibektest.R;
import test.ibek.nurzhan.kz.ibektest.db.Countries;
import test.ibek.nurzhan.kz.ibektest.util.Logger;

public class MyService extends Service {

    Context context;
    Handler handler;
    int mInterval = 60000;
    List<Countries> countriesList;
    Map<String,Integer> notifiedAmount= new ArrayMap<>();

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.i("***************service created*****************");
        context=this;
        handler = new Handler();
        startRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                  countriesList = Countries.findWithQuery(Countries.class, "Select * from Countries");
                  if(countriesList!=null && countriesList.size()>0){
                      for(Countries countries:countriesList){
                          if(countries.getCities()==null){
                              countries.setPopulation(countries.getPopulation()+(int)(countries.getPopulation()*0.1));
                              countries.save();
                              int population = countries.getPopulation();
                              String city = countries.getCapitalCities();
                              if(notifiedAmount.get((String)city)==null){
                                  notifiedAmount.put(city,population);
                              }

                              if(population>5000000 && population<10000000){
                                  if(notifiedAmount.get((String)city)<5000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>10000000 && population<20000000){
                                  if(notifiedAmount.get((String)city)<10000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>20000000 && population<50000000){
                                  if(notifiedAmount.get((String)city)<20000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>50000000 && population<100000000){
                                  if(notifiedAmount.get((String)city)<50000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>100000000){
                                  if(notifiedAmount.get((String)city)<100000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }

                          }else{
                              countries.setPopulation(countries.getPopulation()+(int)(countries.getPopulation()*0.05));
                              countries.save();
                              int population = countries.getPopulation();
                              String city = countries.getCities();
                              if(notifiedAmount.get((String)city)==null){
                                  notifiedAmount.put(city,population);
                              }
                              if(population>5000000 && population<10000000){
                                  if(notifiedAmount.get((String)city)<5000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>10000000 && population<20000000){
                                  if(notifiedAmount.get((String)city)<10000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>20000000 && population<50000000){
                                  if(notifiedAmount.get((String)city)<20000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>50000000 && population<100000000){
                                  if(notifiedAmount.get((String)city)<50000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }else if(population>100000000){
                                  if(notifiedAmount.get((String)city)<100000000){
                                      notifiedAmount.put(city,population);
                                      showNotification(city+"   "+population );
                                  }
                              }
                          }
                      }
                  }

            } finally {
                handler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void notify(String city, int population){
        if(notifiedAmount.get((String)city)==null){
            notifiedAmount.put(city,population);
        }

        if(population>5000000){
            if(notifiedAmount.get((String)city)<5000000){
                notifiedAmount.put(city,population);
                showNotification(city+"   "+population );
            }
        }else if(population>10000000){
            if(notifiedAmount.get((String)city)<10000000){
                notifiedAmount.put(city,population);
                showNotification(city+"   "+population );
            }
        }else if(population>20000000){
            if(notifiedAmount.get((String)city)<20000000){
                notifiedAmount.put(city,population);
                showNotification(city+"   "+population );
            }
        }else if(population>50000000){
            if(notifiedAmount.get((String)city)<50000000){
                notifiedAmount.put(city,population);
                showNotification(city+"   "+population );
            }
        }else if(population>100000000){
            if(notifiedAmount.get((String)city)<100000000){
                notifiedAmount.put(city,population);
                showNotification(city+"   "+population );
            }
        }
    }

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        handler.removeCallbacks(mStatusChecker);
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        stopRepeatingTask();
        Logger.i("***************service destroyed*****************");
    }

    int notIndex=0;
    public void showNotification(String message){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.population_grow_msg))
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(false);

        NotificationManager notificationManager;
        notificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notIndex, notificationBuilder.build());
        notIndex++;
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
