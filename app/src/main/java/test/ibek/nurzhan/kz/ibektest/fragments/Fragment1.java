package test.ibek.nurzhan.kz.ibektest.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import test.ibek.nurzhan.kz.ibektest.R;
import test.ibek.nurzhan.kz.ibektest.services.MyService;
import test.ibek.nurzhan.kz.ibektest.activities.MainActivity;
import test.ibek.nurzhan.kz.ibektest.db.Countries;
import test.ibek.nurzhan.kz.ibektest.db.MigrationHistory;
import test.ibek.nurzhan.kz.ibektest.util.Logger;

/**
 * Created by nurzhan on 9/14/2016.
 */
public class Fragment1 extends Fragment implements AdapterView.OnItemSelectedListener {

    Spinner countrySpinner1;
    Spinner countrySpinner2;
    Spinner citySpinner1;
    Spinner citySpinner2;
    EditText editText;
    Button button;

    ArrayList<String> countriesNamesList = new ArrayList<>();
    Map<String,Integer> populationListA = new ArrayMap<>();
    Map<String,Integer> populationListB = new ArrayMap<>();
    HashSet<String> hashSet=new HashSet<>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1, container, false);

        editText = (EditText) view.findViewById(R.id.editText);
        button = (Button) view.findViewById(R.id.button);
        countrySpinner1 = (Spinner) view.findViewById(R.id.spinner1);
        countrySpinner2 = (Spinner) view.findViewById(R.id.spinner2);
        citySpinner1 = (Spinner) view.findViewById(R.id.spinner);
        citySpinner2 = (Spinner) view.findViewById(R.id.spinner3);
        countrySpinner1.setOnItemSelectedListener(this);
        countrySpinner2.setOnItemSelectedListener(this);
        citySpinner1.setOnItemSelectedListener(this);
        citySpinner2.setOnItemSelectedListener(this);

        // checking if the db already exists
        List<Countries> countriesList = Countries.findWithQuery(Countries.class, "Select * from Countries");
        if(countriesList!=null && countriesList.size()>0){
            for(int i=0; i<countriesList.size();i++){
                hashSet.add(countriesList.get(i).getName());
            }
            countriesNamesList.add("");
            countriesNamesList.addAll(hashSet);
            initSpinners(countriesNamesList);
            Logger.i("***************service start*****************"+((MainActivity)getActivity()).isMyServiceRunning(MyService.class));
            if(!((MainActivity)getActivity()).isMyServiceRunning(MyService.class)){
                getActivity().startService(new Intent(getActivity(), MyService.class));
                Logger.i("***************service start*****************");
            }
        }else {
            //if db does not exists, then retrieving the data and creating new db
            getCountriesTask = new GetCountriesRequest().execute();
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = editText.getText().toString();
                if(!value.equals("")){
                    if(citySpinner2.getSelectedItem()!=null &&citySpinner1.getSelectedItem()!=null){
                        int migrationAmount = Integer.valueOf(editText.getText().toString());
                        String cityA = (String)citySpinner1.getSelectedItem();
                        String cityB = (String)citySpinner2.getSelectedItem();
                        int cityPopulationA = populationListA.get(cityA);
                        int cityPopulationB = populationListB.get(cityB);

                        if(migrationAmount<=cityPopulationA){
                            //writing changes to DB
                            Countries countriesA = Countries.findById(Countries.class,selectedCityA);
                            Countries countriesB = Countries.findById(Countries.class,selectedCityB);
                            countriesA.setPopulation(cityPopulationA-migrationAmount);
                            countriesA.save();
                            countriesB.setPopulation(cityPopulationB+migrationAmount);
                            countriesB.save();
                            populationListA.put((String)citySpinner1.getSelectedItem(),cityPopulationA-migrationAmount);
                            populationListB.put((String)citySpinner2.getSelectedItem(),cityPopulationA+migrationAmount);

                            //writing migration history table
                            new MigrationHistory(cityA,cityB,migrationAmount).save();

                            editText.setText("");

                        }else {
                            String msg = String.format(getString(R.string.migration_msg2),String.valueOf(cityPopulationA));
                            ((MainActivity)getActivity()).showMessage(msg,null);
                        }
                    }else {
                        ((MainActivity)getActivity()).showMessage(getString(R.string.migration_msg3),null);
                    }
                }else {
                    ((MainActivity)getActivity()).showMessage(getString(R.string.migration_msg),null);
                }
            }
        });


        return view;
    }

    void initSpinners(ArrayList<String> list){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner1.setAdapter(adapter);
        countrySpinner2.setAdapter(adapter);
    }

    void initCitySpinner(Spinner spinner, ArrayList<String> list){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    Map<String, Long> citiesA = new ArrayMap<>();
    Map<String, Long> citiesB = new ArrayMap<>();
    Long selectedCityA;
    Long selectedCityB;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch (adapterView.getId()) {
            case R.id.spinner1:
                if(!countriesNamesList.get(i).equals("")){
                    List<Countries> countriesListA = Countries.find(Countries.class,"name=?",countriesNamesList.get(i));
                    ArrayList<String> cities = new ArrayList<>();
                    citiesA.clear();
                    populationListA.clear();
                    for(Countries c : countriesListA){
                        if(c.getCities()!=null){
                            cities.add(c.getCities());
                            citiesA.put(c.getCities(),c.getId());
                            populationListA.put(c.getCities(),c.getPopulation());
                        }else {
                            cities.add(c.getCapitalCities());
                            citiesA.put(c.getCapitalCities(),c.getId());
                            populationListA.put(c.getCapitalCities(),c.getPopulation());
                        }
                    }
                    Collections.sort(cities);
                    initCitySpinner(citySpinner1,cities);
                }
                break;

            case R.id.spinner2:
                if(!countriesNamesList.get(i).equals("")){
                    List<Countries> countriesListB = Countries.find(Countries.class,"name=?",countriesNamesList.get(i));
                    ArrayList<String> cities = new ArrayList<>();
                    citiesB.clear();
                    populationListB.clear();
                    for(Countries c : countriesListB){
                        if(c.getCities()!=null){
                            cities.add(c.getCities());
                            citiesB.put(c.getCities(),c.getId());
                            populationListB.put(c.getCities(),c.getPopulation());
                        }else {
                            cities.add(c.getCapitalCities());
                            citiesB.put(c.getCapitalCities(),c.getId());
                            populationListB.put(c.getCapitalCities(),c.getPopulation());
                        }
                    }
                    Collections.sort(cities);
                    initCitySpinner(citySpinner2,cities);
                }
                break;


            case R.id.spinner:
                if(citiesA.size()>0){
                    selectedCityA = citiesA.get((String) citySpinner1.getSelectedItem());
                }
                break;

            case R.id.spinner3:
                if(citiesA.size()>0){
                    selectedCityB = citiesB.get((String) citySpinner2.getSelectedItem());
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getCountriesTask!=null){getCountriesTask.cancel(true);}
        //getActivity().stopService(new Intent(getActivity(), MyService.class));
    }

    //--------------------------------------get countries&cities------------------------------------
    AsyncTask<Void, Void, String> getCountriesTask;
    private class GetCountriesRequest extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute(){
            ((MainActivity)getActivity()).showWaitDialog();
        }
        @Override
        protected String doInBackground(Void... params) {return getCountries();
        }

        protected void onPostExecute(String results) {
            ((MainActivity)getActivity()).hideWaitDialog();
            if (results!=null) {
                try {

                    Countries.deleteAll(Countries.class);
                    countriesNamesList.clear();
                    countriesNamesList.add("");

                    JSONObject jsonObject = new JSONObject(results);
                    JSONArray countriesArray = jsonObject.getJSONArray("countries");
                    for(int i=0; i<countriesArray.length();i++){

                        JSONObject capital = countriesArray.getJSONObject(i).getJSONObject("capital");
                        Countries countries = new Countries(countriesArray.getJSONObject(i).getString("name"),
                                null,capital.getString("name"),capital.getInt("people"));
                        countries.save();
                        countriesNamesList.add(countriesArray.getJSONObject(i).getString("name"));

                        JSONArray citiesArray = countriesArray.getJSONObject(i).getJSONArray("cities");
                        for(int j=0; j<citiesArray.length();j++){
                            new Countries(countriesArray.getJSONObject(i).getString("name"),
                                    citiesArray.getJSONObject(j).getString("name"),null,
                                    citiesArray.getJSONObject(j).getInt("people")).save();
                         }
                    }

                    if(!((MainActivity)getActivity()).isMyServiceRunning(MyService.class)){
                        getActivity().startService(new Intent(getActivity(), MyService.class));
                    }
                    initSpinners(countriesNamesList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else {
                ((MainActivity)getActivity()).showConnectionErrorMessage(getString(R.string.connection_error_message), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getCountriesTask = new GetCountriesRequest().execute();
                    }
                });
            }

            /* List<Countries> countriesNamesList= Countries.find(Countries.class,"name=?","USA");
             Logger.i("*********countriesNamesList**********"+countriesNamesList.size());
             Logger.i("*********countriesNamesList**********"+countriesNamesList.get(0).getName());
             Logger.i("*********countriesNamesList**********"+countriesNamesList.get(0).getCities());*/

        }
    }

    String getCountries () {
        URL url = null;
        try {

            url = new URL("http://ibecsystems.com/test.json");
            Logger.i("Request: " + url.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");

            String response;
            try {
                int code = conn.getResponseCode();
                Logger.i("Response Code: " + code+" "+conn.getResponseMessage());

                InputStream in;
                if(code<400){
                    in = new BufferedInputStream(conn.getInputStream());
                }else{
                    in = new BufferedInputStream(conn.getErrorStream());
                }
                response = IOUtils.toString(in, "UTF-8");
                Logger.i("Response: "+response);
            }
            finally {
                conn.disconnect();
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    //----------------------------------------------------------------------------------------------
}
