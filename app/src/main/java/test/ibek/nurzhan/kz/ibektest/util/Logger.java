package test.ibek.nurzhan.kz.ibektest.util;

import android.util.Log;

/**
 * Created by nurzhan on 9/14/2016.
 */
public class Logger {
    public static void i(String name, String msg) {
        Log.i(name, msg);
    }

    public static void i(String msg) {
        if(msg.length() > 1000) {
            i("Mine", msg.substring(0, 1000));
            i(msg.substring(1000));
        } else
            i("Mine", msg);
    }
}
