package test.ibek.nurzhan.kz.ibektest.db;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by nurzhan on 9/14/2016.
 */
@Table
public class Countries extends SugarRecord {
    private Long id;
    private String name;
    private String cities;
    private String capitalCities;
    private int population;

    public Countries() {
    }

    public Countries(String name, String cities, String capitalCities, int population) {
        this.name = name;
        this.cities = cities;
        this.capitalCities = capitalCities;
        this.population = population;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getCapitalCities() {
        return capitalCities;
    }

    public void setCapitalCities(String capitalCities) {
        this.capitalCities = capitalCities;
    }
}
