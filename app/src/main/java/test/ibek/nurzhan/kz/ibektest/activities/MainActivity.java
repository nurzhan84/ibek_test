package test.ibek.nurzhan.kz.ibektest.activities;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import test.ibek.nurzhan.kz.ibektest.R;
import test.ibek.nurzhan.kz.ibektest.dialogs.WaitDialog;
import test.ibek.nurzhan.kz.ibektest.fragments.Fragment1;
import test.ibek.nurzhan.kz.ibektest.fragments.Fragment2;
import test.ibek.nurzhan.kz.ibektest.util.Logger;

public class MainActivity extends AppCompatActivity {

    WaitDialog waitDialog;
    LinearLayout container1,container2;
    Fragment1 fragment1;
    Fragment2 fragment2;
    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container1 = (LinearLayout) findViewById(R.id.container1);
        container2 = (LinearLayout) findViewById(R.id.container2);

        waitDialog = new WaitDialog();
        waitDialog.setCancelable(false);


        fragmentManager =getSupportFragmentManager();
        if (savedInstanceState == null) {
            fragment1 = new Fragment1();
            fragmentManager.beginTransaction().replace(container1.getId(),fragment1,"fragment1").commit();
            fragment2 = new Fragment2();
            fragmentManager.beginTransaction().replace(container2.getId(),fragment2,"fragment2").commit();
        }else {
            fragment1 = (Fragment1) fragmentManager.getFragment(savedInstanceState, "fragment1");
            fragmentManager.beginTransaction().replace(container1.getId(),fragment1,"fragment1").commit();
            fragment2 = (Fragment2) fragmentManager.getFragment(savedInstanceState, "fragment2");
            fragmentManager.beginTransaction().replace(container2.getId(),fragment2,"fragment2").commit();
        }


    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logger.i("*********onSaveInstanceState*********");
        fragmentManager.putFragment(outState, "fragment1", fragment1);
        fragmentManager.putFragment(outState, "fragment2", fragment2);
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void showWaitDialog(){
        waitDialog.show(getSupportFragmentManager(),"waitDialog");
    }
    public void hideWaitDialog(){
        waitDialog.dismiss();
    }

    public void showMessage(String message, DialogInterface.OnDismissListener onDismissListener){
        try{
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            alertDialog.setOnDismissListener(onDismissListener);
        }catch (Exception e){}

    }
    public void showConnectionErrorMessage(String message, final DialogInterface.OnDismissListener onDismissListener, final DialogInterface.OnClickListener tryAgain){
        try{
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if(onDismissListener!=null){
                                onDismissListener.onDismiss(dialog);
                            }
                        }
                    })
                    .setNegativeButton(getString(R.string.try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (tryAgain!=null) {
                                tryAgain.onClick(dialogInterface,i);
                            }
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
           // alertDialog.setOnDismissListener(onDismissListener);

        }catch (Exception e){}

    }

}
