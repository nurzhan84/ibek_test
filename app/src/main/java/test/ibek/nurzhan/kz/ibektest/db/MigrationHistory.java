package test.ibek.nurzhan.kz.ibektest.db;

import com.orm.SugarRecord;

/**
 * Created by nurzhan on 9/14/2016.
 */
public class MigrationHistory extends SugarRecord {
    private String cityA;
    private String cityB;
    private int Amount;

    public MigrationHistory() {
    }

    public MigrationHistory(String cityA, String cityB, int amount) {
        this.cityA = cityA;
        this.cityB = cityB;
        Amount = amount;
    }

    public String getCityA() {
        return cityA;
    }

    public void setCityA(String cityA) {
        this.cityA = cityA;
    }

    public String getCityB() {
        return cityB;
    }

    public void setCityB(String cityB) {
        this.cityB = cityB;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }
}
