package test.ibek.nurzhan.kz.ibektest.dialogs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import test.ibek.nurzhan.kz.ibektest.R;

/**
 * Created by nurzhan on 9/14/2016.
 */
public class WaitDialog extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_wait, container, false);

        return view;
    }

}
