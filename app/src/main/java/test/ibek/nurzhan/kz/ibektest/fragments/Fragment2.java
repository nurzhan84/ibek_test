package test.ibek.nurzhan.kz.ibektest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import test.ibek.nurzhan.kz.ibektest.R;
import test.ibek.nurzhan.kz.ibektest.activities.MigrationHistoryActivity;

/**
 * Created by nurzhan on 9/14/2016.
 */
public class Fragment2 extends Fragment {

    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2, container, false);

        button = (Button) view.findViewById(R.id.button2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(), MigrationHistoryActivity.class));
            }
        });

        return view;
    }
}
