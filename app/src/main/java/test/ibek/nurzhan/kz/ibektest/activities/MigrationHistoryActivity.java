package test.ibek.nurzhan.kz.ibektest.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import test.ibek.nurzhan.kz.ibektest.R;
import test.ibek.nurzhan.kz.ibektest.db.Countries;
import test.ibek.nurzhan.kz.ibektest.db.MigrationHistory;
import test.ibek.nurzhan.kz.ibektest.util.Logger;

public class MigrationHistoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Adapter adapter;

    ArrayList<String> cityA = new ArrayList<>();
    ArrayList<String> cityB = new ArrayList<>();
    ArrayList<Integer> amount = new ArrayList<>();
    ArrayList<ArrayList<String>> itemData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_migration_history);

        try{
            List<MigrationHistory> migrationHistoryList = MigrationHistory.findWithQuery(MigrationHistory.class, "Select * from migration_history");
            for(MigrationHistory mg : migrationHistoryList){
                cityA.add(mg.getCityA());
                cityB.add(mg.getCityB());
                amount.add(mg.getAmount());
            }
        }catch (Exception e){
            //table does not exists yet
        }

        itemData.add(cityA);
        itemData.add(cityB);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new Adapter(this,itemData);
        recyclerView.setAdapter(adapter);
    }

    //----------------------------------Adapter-----------------------------------------------------
    private class Adapter extends RecyclerView.Adapter<Holder>{

        private Context context;
        private ArrayList<ArrayList<String>> item_data;

        public Adapter(Context c , ArrayList<ArrayList<String>> itemList) {
            context=c;
            item_data = itemList;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_history, parent, false);
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(Holder viewHolder, int position) {
            viewHolder.cityA_tv.setText(item_data.get(0).get(position));
            viewHolder.cityB_tv.setText(item_data.get(1).get(position));
            viewHolder.amount_tv.setText(String.valueOf(amount.get(position)));
        }

        @Override
        public int getItemCount() {
            return item_data.get(0).size();
        }
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView cityA_tv,cityB_tv,amount_tv;

        public Holder(View itemView) {
            super(itemView);
            cityA_tv = (TextView) itemView.findViewById(R.id.textView6);
            cityB_tv = (TextView) itemView.findViewById(R.id.textView8);
            amount_tv = (TextView) itemView.findViewById(R.id.textView7);
        }

    }
    //----------------------------------------------------------------------------------------------
}
